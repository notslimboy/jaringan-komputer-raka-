//Server
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class UdpSrvrSample
{
    public static void Main()
    {
        string input;
        byte[] data = new byte[1024]; 
        byte[] data1 = new byte[1024];
        IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 8001); // DEKLARASI ENDPOINT DI IP BEBAS DAN DI PORT 8001
        UdpClient newsock = new UdpClient(ipep); //DEKLARASI SOCKET UDP 

        // UDP PROTOCOL
        Socket client = new Socket(AddressFamily.InterNetwork,
                       SocketType.Dgram, ProtocolType.Udp); 

        // INPUT USERNAME
        Console.WriteLine("Menunggu koneksi dari klien...");
        Console.Write("Masukkan username anda : ");
        string username = Console.ReadLine();

        IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);

       // MENERIMA DATA
        data = newsock.Receive(ref sender);
        string clientUsername = Encoding.ASCII.GetString(data, 0, data.Length); 

        Console.WriteLine("Menerima pesan dari {0}:", sender.ToString()); // MENERIMA PESAN DARI CLIENT
        Console.WriteLine("Username Client : " + Encoding.ASCII.GetString(data, 0, data.Length));

        data = Encoding.ASCII.GetBytes(username); 
        newsock.Send(data, data.Length, sender);

        // MENERIMA DAN MENGIRIM
        while (true)
        {   
            data = newsock.Receive(ref sender);
            Console.WriteLine(clientUsername + " : " + Encoding.ASCII.GetString(data, 0, data.Length));
            Console.Write(username + " : ");
            input = Console.ReadLine();
            data1 = Encoding.ASCII.GetBytes(input);
            newsock.Send(data1, data1.Length, sender);

        }
    }
}