//Client
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class SimpleUdpClient
{
   public static void Main()
   {
      byte[] data = new byte[1024];
      string input, stringData;
      IPEndPoint ipep = new IPEndPoint(
                      IPAddress.Parse("127.0.0.1"), 8001); 

      // UDP PROTOCOL 
      Socket server = new Socket(AddressFamily.InterNetwork,
           
                     SocketType.Dgram, ProtocolType.Udp);
      // INPPUT USERNAME
      Console.Write("Masukkan username anda : ");
      string username = Console.ReadLine();
      data = Encoding.ASCII.GetBytes(username);
      server.SendTo(data, data.Length, SocketFlags.None, ipep);

      IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
      EndPoint Remote = (EndPoint)sender;

      data = new byte[1024];
      int recv = server.ReceiveFrom(data, ref Remote);

      Console.WriteLine("Menerima pesan dari {0}:", Remote.ToString()); // MENERIMA PESAN DARI SERVER
      string serverUsername = Encoding.ASCII.GetString(data, 0, recv);
      Console.WriteLine("Username Server : " + serverUsername);

      while(true)
      {
      	Console.Write(username + " : ");
         input = Console.ReadLine();
         if (input == "exit") // JIKA CLIENT INPUT EXIT MAKA AKAN DISCONNECTED DARI SERVER
            break;
         server.SendTo(Encoding.ASCII.GetBytes(input), Remote);
         data = new byte[1024];
         recv = server.ReceiveFrom(data, ref Remote);
         stringData = Encoding.ASCII.GetString(data, 0, recv);
         Console.WriteLine(serverUsername + " : " + stringData);
      }
      Console.WriteLine("Menghentikan klien"); 
      Console.ReadLine();
      server.Close(); // DISCONNECTED FROM SERVER 
   }
}
